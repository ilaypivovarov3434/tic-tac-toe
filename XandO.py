class WrongUserInputError(Exception):
    pass
class OutOfTimeError(Exception):
    pass



e = 0
R = [1, 2, 3, 4, 5, 6, 7, 8, 9]
i = 1
hod = {"1": "1", "2": "2", "3": "3", "4": "4", "5": "5", "6": "6", "7": "7", "8": "8", "9": "9"}
l = 0
x = ""
pc = ""
p = 0
def plan():
    print(" _ _ _ _ _ _ _ _ _ ")
    print("|     |     |     |")
    print("|", "", hod.get('1'), " |", "", hod.get('2'), " |", "", hod.get('3'), " |")
    print("|_ _ _|_ _ _|_ _ _|")
    print("|     |     |     |")
    print("|", "", hod.get('4'), " |", "", hod.get('5'), " |", "", hod.get('6'), " |")
    print("|_ _ _|_ _ _|_ _ _|")
    print("|     |     |     |")
    print("|", "", hod.get('7'), " |", "", hod.get('8'), " |", "", hod.get('9'), " |")
    print("|_ _ _|_ _ _|_ _ _|")
def decor1(function_to_decorate):
    def wrapper(e, R, pc):
        print("Сделайте Ход!")
        function_to_decorate(e, R, pc)
        print("Ход сделан!")
    return wrapper
@decor1
def ded(e, R, pc):
    while e == 0:
        try:
            p = int(input())
            if p in R:
                 R.remove(p)
                 p = str(p)
                 hod[p] = pc
                 break
        except p not in R: raise WrongUserInputError

while l < 9:

    x = ""

    plan()
    if l % 2 == 0:
        pc = ("X")
    else:
        pc = ("O")
    l = l + 1

    ded(e, R, pc)
    n = 1
    a = 1
    x = ""
    def win_position(n, hod, a, x):
        for h in range(3):
            if hod.get(str(n)) == hod.get(str(n+1)) == hod.get(str(n+2)):
                x = hod.get(str(n))
                break
            if hod.get(str(a)) == hod.get(str(a+3)) == hod.get(str(a+6)):
                x = hod.get(str(a))
                break
            if hod.get("1") == hod.get("5") == hod.get("9"):
                x = hod.get("1")
                break
            if hod.get("3") == hod.get("5") == hod.get("7"):
                x = hod.get("3")
                break
            n = n+1
            a = a+1
    if l >= 9: raise OutOfTimeError
plan()

if x == "":
    x = "Победителей нет!"

print("Игра окончена!")
print("Победитель:", x)